#include <iostream>

struct Player
{
	std::string name;
	int score;
};
void sortNames(Player* players, int length)
{
	for (int startIndex = 0; startIndex < length; ++startIndex)
	{
		int largestIndex = startIndex;
		for (int currentIndex = startIndex + 1; currentIndex < length; ++currentIndex)
		{
			if (players[currentIndex].score > players[largestIndex].score)
				largestIndex = currentIndex;
		}
		std::swap(players[startIndex], players[largestIndex]);
	}
}

int main()
{
	int numPlayers = 0;
	do
	{
		std::cout << "How many players do you want to add?";
		std::cin >> numPlayers;
	} while (numPlayers <= 1);
	Player* players = new Player[numPlayers];
	for (int index = 0; index < numPlayers; ++index)
	{
		std::cout << "Enter name of player #" << index + 1 << ": ";
		std::cin >> players[index].name;
		std::cout << "Enter player's score #" << index + 1 << ": ";
		std::cin >> players[index].score;
	}
	sortNames(players, numPlayers);
	for (int index = 0; index < numPlayers; ++index)
		std::cout << players[index].name << " score is " << players[index].score << "\n";
	delete[] players;

	return 0;
}